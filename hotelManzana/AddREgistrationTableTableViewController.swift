//
//  AddREgistrationTableTableViewController.swift
//  hotelManzana
//
//  Created by umar on 10/20/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import UIKit

class AddREgistrationTableTableViewController: UITableViewController,SelectRoomTypeTableViewControllerDelegate{
    
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    func didSelect(roomType: RoomType) {
        self.roomType = roomType
        updateRoomType()
    }
    
    var roomType: RoomType?
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBAction func doneButtonTapped(_ sender: UIBarButtonItem) {
        
        let firstname = firstName.text ?? ""
        let lastname = lastName.text ?? ""
        let Email = email.text ?? ""
        
        let checkInDate =  checkInDatePicker.date
        let checkOutDate = checkOutDatePicker.date
        
        let numberOfAdults = Int(numberOfAdultStepper.value)
        let numberOFChildren = Int(numberOfChildrenStepper.value)
        
        let hasWifi = wifiSwitch.isOn
        
        let roomChoice = roomType?.name ?? "not set"
        
        print("DONE TAPPED")
        print("first name : \(firstname)" )
        print("last name : \(lastname)" )
        print("email name : \(Email)" )
        print("check-in date : \(checkInDate)" )
        print("check-out date : \(checkOutDate)" )
        print("number of adult : \(numberOfAdults)" )
        print("number of children : \(numberOFChildren)" )
        print("WiFi : \(hasWifi)" )
        print("room type : \(roomChoice)" )
        
        
    }
    
    @IBOutlet weak var checkInDateLabel: UILabel!
    @IBOutlet weak var checkInDatePicker: UIDatePicker!
    @IBOutlet weak var checkOutDateLabel: UILabel!
    @IBOutlet weak var checkOutDatePicker: UIDatePicker!
    
    @IBAction func DatePickerValue(_ sender: UIDatePicker) {
        updateDateViews()
    }
    
    func updateDateViews() {
        checkOutDatePicker.minimumDate = checkInDatePicker.date.addingTimeInterval(86400)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        
        checkInDateLabel.text = dateFormatter.string(from: checkInDatePicker.date)
        checkOutDateLabel.text = dateFormatter.string(from: checkOutDatePicker.date)
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let midnightToday = Calendar.current.startOfDay(for: Date())
        checkInDatePicker.minimumDate = midnightToday
        checkInDatePicker.date = midnightToday
        
        updateNumerOfGuest()
        
        updateRoomType()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    let checkInDatePickerCellIndexPath = IndexPath(row: 1, section: 1)   // mengatur index untuk cell date picker
    let checkOutDatePickerCellIndexPath = IndexPath(row: 3, section: 1)  // sama dengan yang atas
    
    var isCheckInDatePickerShown: Bool = false {    // computed property untuk membuat hidden dan tidaknya date picker dan defaultnya adalah false, jadi tidak hidden
        didSet {                            // memberi arahan  bahwa nanti isCheckInDatePicker akan di rubah
            checkInDatePicker.isHidden = !isCheckInDatePickerShown
        }
    }
    
    var isCheckoutDatePickerShown: Bool = false {
        didSet {
            checkOutDatePicker.isHidden = !isCheckoutDatePickerShown
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { // fungsi dari swift untuk mengatur height table view
        switch (indexPath.section, indexPath.row) {
        case (checkInDatePickerCellIndexPath.section, checkInDatePickerCellIndexPath.row):
            if isCheckInDatePickerShown {
                return 216.0
            }else{
                return 0.0
            }
            
        case (checkOutDatePickerCellIndexPath.section, checkOutDatePickerCellIndexPath.row ):
            if isCheckoutDatePickerShown {
                return 216.0
            }else{
                return 0.0
            }
        default:
            return 44.0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt
        indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
         
        switch (indexPath.section, indexPath.row) {
        case (checkInDatePickerCellIndexPath.section,
              checkInDatePickerCellIndexPath.row - 1):
             
            if isCheckInDatePickerShown {
                isCheckInDatePickerShown = false
            } else if isCheckoutDatePickerShown {
                isCheckoutDatePickerShown = false
                isCheckInDatePickerShown = true
            } else {
                isCheckInDatePickerShown = true
            }
             
            tableView.beginUpdates()
            tableView.endUpdates()
             
        case (checkOutDatePickerCellIndexPath.section,
              checkOutDatePickerCellIndexPath.row - 1):
            if isCheckoutDatePickerShown {
                isCheckoutDatePickerShown = false
            } else if isCheckInDatePickerShown {
                isCheckInDatePickerShown = false
                isCheckoutDatePickerShown = true
            } else {
                isCheckoutDatePickerShown = true
            }
             
            tableView.beginUpdates()
            tableView.endUpdates()
             
        default:
            break
        }
    }
    
    
    @IBOutlet weak var numberOfAdultStepper: UIStepper!
    @IBOutlet weak var numberOfAdultLabel: UILabel!
    @IBOutlet weak var numberOfChildrenStepper: UIStepper!
    @IBOutlet weak var numberOfChildrenLabel: UILabel!
    
    func updateNumerOfGuest() {
        
        
        numberOfAdultLabel.text = "\(Int(numberOfAdultStepper.value))"
        numberOfChildrenLabel.text = "\(Int(numberOfChildrenStepper.value))"
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) { // merespon untuk perubahan yang dibuat oleh stepper, bisa juga memanggil method updateNumberOfGuest dari viewDidLoad
        
        updateNumerOfGuest()
    }
    
    @IBOutlet weak var wifiSwitch: UISwitch!
    
    @IBAction func wifiSwitchChanged(_ sender: UISwitch) {
        
    }
    
    
    @IBOutlet weak var roomTypeLabel: UILabel!

    func updateRoomType() {
        if let roomType = roomType {
            roomTypeLabel.text = roomType.name
        }else{
            roomTypeLabel.text = "Not Set"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectRoomType" {
            let destinationViewController = segue.destination as? SelectRoomTypeTableViewController
            destinationViewController?.delegate = self
            destinationViewController?.roomType = roomType
        }
    }
    
    var registration: Registration? {
        
        guard let roomType = roomType else {
            return nil
        }
        
        let firstname = firstName.text ?? ""
        let lastname = lastName.text ?? ""
        let Email = email.text ?? ""
        let checkInDate =  checkInDatePicker.date
        let checkOutDate = checkOutDatePicker.date
        let numberOfAdults = Int(numberOfAdultStepper.value)
        let numberOFChildren = Int(numberOfChildrenStepper.value)
        let hasWifi = wifiSwitch.isOn
        let roomChoice = roomType
        
        return Registration(firstName: firstname,
                            lastName: lastname,
                            email: Email,
                            checkInDate: checkInDate,
                            checkOutDate: checkOutDate,
                            numberOfAdult: numberOfAdults,
                            numberOfChildren: numberOFChildren,
                            roomType: roomChoice,
                            WiFi: hasWifi)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}






















// MARK -> boilerplate



/*
 override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
 
 // Configure the cell...
 
 return cell
 }
 */

/*
 // Override to support conditional editing of the table view.
 override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
 // Return false if you do not want the specified item to be editable.
 return true
 }
 */

/*
 // Override to support editing the table view.
 override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
 if editingStyle == .delete {
 // Delete the row from the data source
 tableView.deleteRows(at: [indexPath], with: .fade)
 } else if editingStyle == .insert {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
 
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
 // Return false if you do not want the item to be re-orderable.
 return true
 }
 */

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */


